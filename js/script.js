/* Author: myriam puchalt

*/
//intervalo tiempo cambio img slide, 5 segundos, se llama a la función "avanzaSlide()"
// setInterval('avanzaSlide()',5000);
 
//array con las clases para las diferentes imagenes
// var arrayImagenes = new Array("img1","img2","img3", "img4");
 
//variable que nos permitirá saber qué imagen se está mostrando
var imagecount = 1;
var total= 7;
 
function slide(x){
	
	var Image = document.getElementById("img");
	imagecount += x;
	if(imagecount > total){imagecount = 1;}
	if(imagecount < 1){imagecount = total;}
	Image.src = "img/img"+ imagecount +".jpg";
}

window.setInterval(function slideA() {
	
	var Image = document.getElementById("img");
	imagecount += 1;
	if(imagecount > total){imagecount = 1;}
	if(imagecount < 1){imagecount = total;}
	Image.src = "img/img"+ imagecount +".jpg";
}, 3000);
